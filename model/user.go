package model

type Uuid string

type User struct {
	Name string `json:"name"`
}

type UserData struct {
	Uuid string `json:"uuid" gorm:"primary_key"`
	Name string `json:"name"`
}

type UserCreateResp struct {
	Err  string
	Uuid string
}
