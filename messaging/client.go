package messaging

import (
	"crypto/tls"
	"fmt"

	"github.com/nats-io/go-nats"
)

type Config struct {
	Addr      string
	TlsConfig *tls.Config
}

type NatsClient struct {
	*nats.EncodedConn
}

func New(c *Config) *NatsClient {
	dsn := fmt.Sprintf("nats://%s", c.Addr)
	nc, err := nats.Connect(dsn)
	if err != nil {
		panic(fmt.Sprintf("failed to connect to '%s': %s", dsn, err))
	}
	ec, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		panic(fmt.Sprintf("failed to get encoded conn: %s", err))
	}
	return &NatsClient{ec}
}
