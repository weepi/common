package utils

import (
	"reflect"
	"strings"
)

func StructToMap(v interface{}, tag string) map[string]interface{} {
	rv := reflect.ValueOf(v).Elem()
	numFields := rv.NumField()
	m := make(map[string]interface{})
	for i := 0; i < numFields; i++ {
		tag := rv.Type().Field(i).Tag.Get(tag)
		if tag == "" || tag == "-" {
			continue
		}
		tag = strings.Split(tag, ",")[0]
		m[tag] = rv.Field(i).Interface()
	}
	return m
}

// Returns spefified struct tags name
func GetStructTags(s interface{}, n string) []string {
	v := reflect.ValueOf(s)
	numFields := v.NumField()
	var out []string
	for i := 0; i < numFields; i++ {
		tag := v.Type().Field(i).Tag.Get(n)
		if tag == "" || tag == "-" {
			continue
		}
		out = append(out, tag)
	}
	return out
}
