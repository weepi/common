// Code generated by protoc-gen-go. DO NOT EDIT.
// source: protobuf/common.proto

/*
Package common is a generated protocol buffer package.

It is generated from these files:
	protobuf/common.proto

It has these top-level messages:
*/
package common

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Order int32

const (
	Order_ASC  Order = 0
	Order_DESC Order = 1
)

var Order_name = map[int32]string{
	0: "ASC",
	1: "DESC",
}
var Order_value = map[string]int32{
	"ASC":  0,
	"DESC": 1,
}

func (x Order) String() string {
	return proto.EnumName(Order_name, int32(x))
}
func (Order) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func init() {
	proto.RegisterEnum("common.Order", Order_name, Order_value)
}

func init() { proto.RegisterFile("protobuf/common.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 81 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x2d, 0x28, 0xca, 0x2f,
	0xc9, 0x4f, 0x2a, 0x4d, 0xd3, 0x4f, 0xce, 0xcf, 0xcd, 0xcd, 0xcf, 0xd3, 0x03, 0xf3, 0x85, 0xd8,
	0x20, 0x3c, 0x2d, 0x29, 0x2e, 0x56, 0xff, 0xa2, 0x94, 0xd4, 0x22, 0x21, 0x76, 0x2e, 0x66, 0xc7,
	0x60, 0x67, 0x01, 0x06, 0x21, 0x0e, 0x2e, 0x16, 0x17, 0xd7, 0x60, 0x67, 0x01, 0xc6, 0x24, 0x36,
	0xb0, 0x52, 0x63, 0x40, 0x00, 0x00, 0x00, 0xff, 0xff, 0x6f, 0xa9, 0x10, 0xcc, 0x43, 0x00, 0x00,
	0x00,
}
