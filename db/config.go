package db

type Config struct {
	Driver   string
	Host     string
	User     string
	Name     string
	SSLMode  string
	Password string
	Debug    bool
}
