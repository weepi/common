PROTO_FOLDER 	= protobuf
PROTO_FILE_NAME	= common.proto

help:
	@echo "proto - Generate go files of .proto in $(PROTO_FOLDER) folder"

proto:
	protoc $(PROTO_FOLDER)/$(PROTO_FILE_NAME) --go_out=:.
