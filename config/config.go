package config

import (
	"fmt"

	"gitlab.com/weepi/common/utils"

	"github.com/caarlos0/env"
)

type Configer interface {
	Init(interface{})
	Get(string) string
}

type Config struct {
	m map[string]interface{}
}

func (c *Config) Init(v interface{}) {
	if err := env.Parse(v); err != nil {
		panic(fmt.Sprintf("could not get conf: %s", err))
	}
	c.m = utils.StructToMap(v, "env")
}

func (c *Config) Get(k string) string {
	s, ok := c.m[k].(string)
	if !ok {
		return ""
	}
	return s
}
